from flask import Flask
from flask_restful import Api, Resource
from priceService import PricePoint
import pickle
import redis
import os
import numpy as np

app = Flask(__name__)
api = Api(app)


# Replaces with our configuration information
redis_host = os.environ['REDIS_HOST']
redis_port = int(os.environ['REDIS_PORT'])
redis_password = os.environ['REDIS_PWD']

# redis_host = "localhost"
# redis_port = 6379
# redis_password = ""

redis_cg = redis.Redis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
historical_demand = pickle.load(open("historical_demand.p", "rb"))
slot_sizes = np.sort(historical_demand.total_slots.unique())


class HealthCheck(Resource):
    def get(self):
        return {'health': 'Green'}


api.add_resource(PricePoint, '/dp-fantasy-peak-api',
                 resource_class_kwargs={'redis_head': redis_cg, 'exp_demand': historical_demand, 'slots': slot_sizes})

# api.add_resource(AddContest, '/contest-fantasy-peak-api',
#                  resource_class_kwargs = {'redis_head': redis_cg})

api.add_resource(HealthCheck, '/health-check')

if __name__ == '__main__':
    host_name = '0.0.0.0'
    app.run(host = host_name, debug = False)
