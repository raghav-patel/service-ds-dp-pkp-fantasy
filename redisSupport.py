import redis
import numpy as np
import struct
import ast
import os

# Write are only to make it work and test out the flask API.
# Wont be needed for main implementation


def write_array(a):
    # Price points
    h = a.shape
    shape = struct.pack('I', h[0])
    encoded = shape + a.tobytes()
    return encoded


def closest(num, arr):
    curr = arr[0]
    for val in arr:
        if abs (num - val) < abs (num - curr):
            curr = val
    return curr


def write_redis(r):
    cid = "1234"
    alpha = [50, 48, 46, 44, 40]
    beta = [50, 52, 54, 56, 60]
    arms = [28, 33, 38, 43, 49]
    config = {cid: {'alpha': alpha, 'beta': beta, 'arm_pts': arms}}
    r.set(cid, str(config))


def read_array(array_name):
    arr_n = array_name.encode()
    length = struct.unpack('I', arr_n[:4])
    # Add slicing here, or else the array would differ from the original
    arr_actual = np.frombuffer(arr_n[4:], dtype = int).reshape(length)
    return arr_actual


def read_redis(r, cid):
    config = ast.literal_eval(r.get(cid+'_test'))[str(cid)]
    a = config['exp_dmd']
    b = config['actual_dmd']
    c = config['price_shown']
    d = config['ttm']
    return a, b, c, d


def read_redis_r(r, cid):
    config = ast.literal_eval(r.get(cid))[str(cid)]
    a = config['exp_dmd']
    b = config['actual_dmd']
    c = config['price_shown']
    d = config['ttm']
    return a, b, c, d


# Code to write in redis for testing, will not be called in the actual codebase

# redis_host = os.environ['REDIS_HOST']
# redis_port = int(os.environ['REDIS_PORT'])
# redis_password = os.environ['REDIS_PWD']
#
# redis_cg = redis.Redis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
# write_redis(redis_cg)
