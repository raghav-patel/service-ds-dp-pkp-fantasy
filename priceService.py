from flask import Flask, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse
from redisSupport import read_redis, closest
import numpy as np
import json


class PricePoint(Resource):
    def __init__(self, **kwargs):
        self.redis_head = kwargs['redis_head']
        self.exp_demand = kwargs['exp_demand']
        self.slots = kwargs['slots']

    def post(self):
        """
        :return: price point for the contest
        """
        # args = parser.parse_args()
        json_data = request.get_json(force=True)
        contest_id_str = str(json_data['contest_id'])
        t_slots = int(json_data['total_slots'])
        tt_match = int(json_data['time_to_match'])
        l_prcnt = float(json_data['guardrails_Low'])
        u_prcnt = float(json_data['guardrails_high'])
        lag = int(json_data['lag'])
        vacant_slots = int(json_data['vacant_slots'])
        base_price = int(json_data['base_price'])
        current_price = int(json_data['current_price'])
        l_grdrl = round(l_prcnt*base_price)
        u_grdrl = round(u_prcnt*base_price)
        r = self.redis_head
        try:
            exp_dmd_df = self.exp_demand
            closest_slot = closest(t_slots, self.slots)
            exp_dist = exp_dmd_df[exp_dmd_df.total_slots == closest_slot].groupby('total_slots').mean()
            col_index = int(tt_match) / lag
            exp_dmd = round((1 - exp_dist[col_index].values[0]) * t_slots)
            slope = 1000

            new_price = round((current_price - l_grdrl) + (u_grdrl + l_grdrl) * (
                        1 / (1 + np.exp(-1 * slope * (exp_dmd - vacant_slots) / (t_slots * tt_match)))))

            new_price = max(int((base_price - l_grdrl)), int(new_price))
            new_price = min(int(base_price + u_grdrl), int(new_price))

            try:
                r_exp_dmd, r_act_dmd, r_prcshwn, r_ttm = read_redis(r, contest_id_str)
                r_exp_dmd.extend([exp_dmd])
                r_act_dmd.extend([vacant_slots])
                r_prcshwn.extend([new_price])
                r_ttm.extend([tt_match])
                conf = {str(contest_id_str): {'exp_dmd': r_exp_dmd, 'actual_dmd': r_act_dmd, 'price_shown': r_prcshwn, 'ttm': r_ttm}}
                r.set(contest_id_str+'_test', str(conf))
            # alpha, beta, arm_pts = read_redis(r, contest_id_str, u_seg)
            # beta_params = zip(list(alpha), list(beta), list(arm_pts))
            except ValueError:
                conf = {str(contest_id_str): {'exp_dmd': [0], 'actual_dmd': [0], 'price_shown': [0], 'ttm': [0]}}
                r.set(contest_id_str+'_test', str(conf))

            price_final = {'entry_fee': new_price}
            return jsonify(price_final)
        except ValueError:
            return {'message': "Illegal Value"}, 400
