# Dockerfile - this is a comment. Delete me if you want.
FROM python:3.7
ENV env='dev'
ENV REDIS_HOST='localhost'
ENV REDIS_PORT='6379'
ENV REDIS_PWD=''
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["main.py"]
